package org.example;

import lombok.Data;

import java.util.*;

@Data
public class Main {
    public static void main(String[] args) {
        List<Student> studentList = new ArrayList<>();
        studentList.add(new Student("Minh", 9));
        studentList.add(new Student("Dat", 8));
        studentList.add(new Student("Linh", 7));
        studentList.add(new Student("Son", 6));
        studentList.add(new Student("Cuong", 5));
        studentList.add(new Student("Thang", 4));

        System.out.println("Thu tu hoc sinh theo bang chu cai:");
        studentList.sort(Comparator.comparing(Student::getName));
        studentList.forEach(s -> System.out.println(s.getName() + " co diem trung binh la: " +s.getAverage()));

        //C1
//        studentList.stream()
//                .mapToDouble(Student::getAverage)
//                .average()
//                .ifPresent(avg -> System.out.println("Diem trung binh cua tat ca hoc sinh trong lop la: " + avg));
        //C2
        System.out.println("Diem trung binh cua tat ca hoc sinh trong lop la: " +
                studentList.stream().mapToDouble(Student::getAverage).average().orElse(0));

        Student max = studentList.stream().max((hs1, hs2) -> Double.compare(hs1.getAverage(), hs2.getAverage())).orElse(null);
        System.out.println("Diem trung binh cao nhat la: " + max);

        System.out.print("Tong diem cua hoc sinh co diem trung binh > 5 la: ");
        boolean check = studentList.stream().anyMatch(student -> student.getAverage() > 5);
        if (check) {
            double average5 = studentList.stream().filter(student -> student.getAverage() > 5).mapToDouble(Student::getAverage).sum();
            System.out.println(average5);
        } else {
            System.out.println("Khong co sinh vien nao");
        }

        System.out.print("Danh sach hoc sinh bi dao nguoc la: ");
        Collections.reverse(studentList);
        studentList.forEach(s -> System.out.print(s.getName() + ", "));
    }
}